global _start
extern _binary_input_txt_start

section .text

; esi - unsigned input number
print_number:
  push rbp
  ; use the space after rsp as a buffer
  ; if the number is more then 32 digits then D:
  ; this doesn't change rsp to allocate a buffer on the stash but it should be fine since nothing else uses the stack

  ; creates a buffer on the stack to print
  ; r13 - start of buffer
  ; r14 - total length of buffer
  ; r15 - number to print
  mov r13, rsp;
  xor r14, r14
  mov r15, rsi

  ; move esp to (hopefully) be out of the buffer area during the syscall
  ; this doesn't do add rsp, r14 because that creates alignment issues with other stack allocated values and it takes some amount of work to realign it
  sub rsp, 32

  loop_start:
	; rdx - current digit
	; rdx = number to print % 10
	; r15 /= 10
	mov rax, r15
	xor rdx, rdx
	mov rcx, 10
	div rcx
	mov r15, rax
	add rdx, '0'

	; push the current digit to buffer[stack pointer - number of previously-existing digits]
	; uses minus since stack grows down and also the digits need to be in reverse order than they're processed

	mov [r13], dl
	dec r13
	inc r14

	; if there are more digits to process, loop again
	test r15, r15
	jnz loop_start

  inc r13 ; increment buffer by one since it currently points to a byte before the buffer starts

  ; write(1, buffer, len)
  mov rax, 1
  mov rdi, 1
  mov rsi, r13
  mov rdx, r14
  syscall

  ; restore rsp
  add rsp, 32

  pop rbp
  ret

; rsi - pointer to start of round info after the "Game [id]: " prefix. will point to the byte after the newline when it returns.
; rax - power of minimum required cubes
; clobbers r11-r15
round_power:
  ; r12 - the value 10
  ; r13 - highest red count
  ; r14 - highest green count
  ; r15 - highest blue count
  mov r12, 10
  xor r13, r13
  xor r14, r14
  xor r15, r15

  check_round:
    ; rax - current number
	xor rax, rax

	read_digit:
	  ; r11b - current digit
	  movzx r11, byte [rsi]
	  inc rsi
	  cmp r11b, ' '
	  je read_color

	  ; this byte is a digit
	  sub r11, '0'
	  mul r12
      add rax, r11
	  jmp read_digit

	read_color:
    mov r11b, [rsi]
	cmp r11b, 'g'
	je handle_green
	cmp r11b, 'b'
	je handle_blue

	; assume that the color is red
	cmp r13, rax
	cmovl r13, rax
	add rsi, 3 ; skip "red" in the string
	jmp read_state

	handle_green:
	cmp r14, rax
	cmovl r14, rax
	add rsi, 5 ; skip "green" in the string
	jmp read_state

	handle_blue:
	cmp r15, rax
	cmovl r15, rax
	add rsi, 4 ; skip "blue" in the string

	read_state:
	mov r11b, [rsi]
    cmp r11b, 10 ; check if this is a newline, indicating that the game is over
	je make_result

	; this is a comma or semicolon, read the next step
	add rsi, 2 ; skip ", " in the string
	jmp check_round

  make_result:
  ; calculate cube power
  mov rax, r13
  mul r14
  mul r15

  ; Skip to the byte after the next newline
  inc rsi

  ret
  
_start:
  ; rsi - current string pointer
  ; r8 - sum of cube powers
  ; r9 - length of game id in base 10 digits
  ; r10 - game id
  mov rsi, _binary_input_txt_start
  xor r8, r8
  mov r9, 1
  mov r10, 1

  read_game:
    add rsi, 7 ; skip "Game : " in the string
	add rsi, r9 ; skip the game id in the string
    call round_power
	add r8, rax

    inc r10

	cmp r10, 10
	je increment_game_id_length
	cmp r10, 100
	je increment_game_id_length
	cmp r10, 101
	je print_result
	jmp read_game

	increment_game_id_length:
	inc r9
	jmp read_game

  print_result:
  mov rsi, r8
  call print_number

  ; exit(0)
  mov rax, 60
  mov rdi, 0
  syscall
