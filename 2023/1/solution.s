global _start
extern _binary_input_txt_start
extern maybe_spelled_out

section .text

; esi - unsigned input number
print_number:
  push rbp
  ; use the space after rsp as a buffer
  ; if the number is more then 32 digits then D:
  ; this doesn't change rsp to allocate a buffer on the stash but it should be fine since nothing else uses the stack

  ; creates a buffer on the stack to print
  ; r13 - start of buffer
  ; r14 - total length of buffer
  ; r15 - number to print
  mov r13, rsp;
  xor r14, r14
  mov r15, rsi

  ; move esp to (hopefully) be out of the buffer area during the syscall
  ; this doesn't do add rsp, r14 because that creates alignment issues with other stack allocated values and it takes some amount of work to realign it
  sub rsp, 32

  loop_start:
	; rdx - current digit
	; rdx = number to print % 10
	; r15 /= 10
	mov rax, r15
	xor rdx, rdx
	mov rcx, 10
	div rcx
	mov r15, rax
	add rdx, '0'

	; push the current digit to buffer[stack pointer - number of previously-existing digits]
	; uses minus since stack grows down and also the digits need to be in reverse order than they're processed

	mov [r13], dl
	dec r13
	inc r14

	; if there are more digits to process, loop again
	test r15, r15
	jnz loop_start

  inc r13 ; increment buffer by one since it currently points to a byte before the buffer starts

  ; write(1, buffer, len)
  mov rax, 1
  mov rdi, 1
  mov rsi, r13
  mov rdx, r14
  syscall

  ; restore rsp
  add rsp, 32

  pop rbp
  ret

; rsi - string pointer
; al - resulting parsed digit, 10 if the pointer doesn't point to a valid digit
; clobbers r13-r15
; rsi is set to the next \n
parse_digit:
  push rbp

  ; r15 - current byte
  mov r15b, [rsi]

  ; Jump to maybe_spelled_out if this is not characters 0-9
  cmp r15b, '0'
  jl maybe_spelled_out
  cmp r15b, '9'
  ja maybe_spelled_out

  ; This is a digit, return it as a number
  mov al, r15b
  sub al, '0'
  pop rbp
  ret

; rsi - string pointer in the format defined by day 1
; al - resulting number
; clobbers r11-r15
read_number_from_line:
  push rbp
  ; r11b - first digit
  ; r12b - last digit

  first_digit_loop:
    ; hopefully there's a digit on this line
	call parse_digit
	inc rsi
	cmp al, 10
	je first_digit_loop

  ; the first (and possibly last) digit has been found
  mov r11b, al
  mov r12b, al

  last_digit_loop:
    ; r10b - current char
	mov r10b, [rsi]
	cmp r10b, 10 ; check for \n
	je loop_finish

	call parse_digit
	inc rsi
	cmp al, 10
	cmovne r12w, ax
	jmp last_digit_loop


  loop_finish:
  mov al, 10
  mul r11
  add al, r12b
  pop rbp
  ret

_start:
  ; rsi - current string pointer
  ; r9 - sum of all numbers
  mov rsi, _binary_input_txt_start
  xor r9, r9

  line_loop_start:
      xor rax, rax
	  call read_number_from_line
      add r9, rax
	  inc rsi ; go to next line
	  ; r10b - current byte
	  mov r10b, [rsi]
	  cmp r10b, 10
	  jne line_loop_start

  mov rsi, r9
  call print_number

  ; exit(0)
  mov rax, 60
  mov rdi, 0
  syscall
