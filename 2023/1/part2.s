global maybe_spelled_out

maybe_spelled_out:
  ; r13b - register for cmove
  ; r14d - first 4 bytes of input
  ; This doesn't care about the read going out of bounds of the string. Hopefully it's terminated by something.
  mov r14d, [rsi]
  mov al, 10

  ; Check for digits with at least 4 letters. Only the first 4 letters are checked because it's unlikely that random letters will accidentally form the starting 4 letters and it's too much work to check it correctly.
  mov r13w, 0
  cmp r14d, 'zero'
  cmove ax, r13w

  mov r13w, 3
  cmp r14d, 'thre'
  cmove ax, r13w

  mov r13w, 4
  cmp r14d, 'four'
  cmove ax, r13w

  mov r13w, 5
  cmp r14d, 'five'
  cmove ax, r13w

  mov r13w, 7
  cmp r14d, 'seve'
  cmove ax, r13w

  mov r13w, 8
  cmp r14d, 'eigh'
  cmove ax, r13w

  mov r13w, 9
  cmp r14d, 'nine'
  cmove ax, r13w

  ; Check digits with 3 letters
  and r14d, 0x00FFFFFF

  mov r13w, 1
  cmp r14d, 'one'
  cmove ax, r13w

  mov r13w, 2
  cmp r14d, 'two'
  cmove ax, r13w

  mov r13w, 6
  cmp r14d, 'six'
  cmove ax, r13w

  pop rbp
  ret
