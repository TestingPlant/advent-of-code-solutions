global _start
extern _binary_input_txt_start

section .text

; esi - unsigned input number
print_number:
  push rbp
  ; use the space after rsp as a buffer
  ; if the number is more then 32 digits then D:
  ; this doesn't change rsp to allocate a buffer on the stash but it should be fine since nothing else uses the stack

  ; creates a buffer on the stack to print
  ; r13 - start of buffer
  ; r14 - total length of buffer
  ; r15 - number to print
  mov r13, rsp;
  xor r14, r14
  mov r15, rsi

  ; move esp to (hopefully) be out of the buffer area during the syscall
  ; this doesn't do add rsp, r14 because that creates alignment issues with other stack allocated values and it takes some amount of work to realign it
  sub rsp, 32

  loop_start:
	; rdx - current digit
	; rdx = number to print % 10
	; r15 /= 10
	mov rax, r15
	xor rdx, rdx
	mov rcx, 10
	div rcx
	mov r15, rax
	add rdx, '0'

	; push the current digit to buffer[stack pointer - number of previously-existing digits]
	; uses minus since stack grows down and also the digits need to be in reverse order than they're processed

	mov [r13], dl
	dec r13
	inc r14

	; if there are more digits to process, loop again
	test r15, r15
	jnz loop_start

  inc r13 ; increment buffer by one since it currently points to a byte before the buffer starts

  ; write(1, buffer, len)
  mov rax, 1
  mov rdi, 1
  mov rsi, r13
  mov rdx, r14
  syscall

  ; restore rsp
  add rsp, 32

  pop rbp
  ret

; rsi - pointer to input
; rax - set to score
; clobbers r11-r15
score_card:
  xor rax, rax
  ; r13 - start of selected numbers
  ; r14 - winning number index in rsi
  ; r15 - selected number index in r13
  mov r13, rsi
  add r13, 32
  xor r14, r14

  search_winning_numbers:
    ; r11w - winning number as characters
	mov r11w, [rsi + r14]
	xor r15, r15

	search_selected_numbers:
	  ; r12w - selected number as characters
	  mov r12w, [r13 + r15]
	  cmp r12w, r11w
	  je got_winning_number
	  add r15, 3
	  cmp r15, 75
	  je next_winning_number
	  jmp search_selected_numbers

	got_winning_number:
	cmp rax, 0
	je got_first_winner
	add rax, rax
	jmp next_winning_number

	got_first_winner:
	mov rax, 1

    next_winning_number:
    add r14, 3
	cmp r14, 30
	jne search_winning_numbers

  ret
  
_start:
  ; rsi - current string pointer
  ; r8 - total score
  ; r10 - card id
  mov rsi, _binary_input_txt_start
  xor r8, r8
  mov r10, 1

  read_card:
    add rsi, 10 ; skip "Card ###: " in the string
    call score_card
	add rsi, 107 ; skip to the next line
	add r8, rax

    inc r10

    cmp r10, 212
	je print_result
	jmp read_card

  print_result:
  nop
  mov rsi, r8
  call print_number

  ; exit(0)
  mov rax, 60
  mov rdi, 0
  syscall
