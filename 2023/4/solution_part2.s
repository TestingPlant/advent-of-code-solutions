global _start
extern _binary_input_txt_start

section .text

; esi - unsigned input number
print_number:
  push rbp
  ; use the space after rsp as a buffer
  ; if the number is more then 32 digits then D:
  ; this doesn't change rsp to allocate a buffer on the stash but it should be fine since nothing else uses the stack

  ; creates a buffer on the stack to print
  ; r13 - start of buffer
  ; r14 - total length of buffer
  ; r15 - number to print
  mov r13, rsp;
  xor r14, r14
  mov r15, rsi

  ; move esp to (hopefully) be out of the buffer area during the syscall
  ; this doesn't do add rsp, r14 because that creates alignment issues with other stack allocated values and it takes some amount of work to realign it
  sub rsp, 32

  loop_start:
	; rdx - current digit
	; rdx = number to print % 10
	; r15 /= 10
	mov rax, r15
	xor rdx, rdx
	mov rcx, 10
	div rcx
	mov r15, rax
	add rdx, '0'

	; push the current digit to buffer[stack pointer - number of previously-existing digits]
	; uses minus since stack grows down and also the digits need to be in reverse order than they're processed

	mov [r13], dl
	dec r13
	inc r14

	; if there are more digits to process, loop again
	test r15, r15
	jnz loop_start

  inc r13 ; increment buffer by one since it currently points to a byte before the buffer starts

  ; write(1, buffer, len)
  mov rax, 1
  mov rdi, 1
  mov rsi, r13
  mov rdx, r14
  syscall

  ; restore rsp
  add rsp, 32

  pop rbp
  ret

_start:
  ; the card ids in the input start at 1, but for the rest of the comments and code they'll be assumed to start at 0
  ; this pushes and popss on the stack
  ; rsp will be unaligned during this but it doesn't matter since no functions are being called
  ; r14 - total number of cards
  ; r15 - original rsp value
  ; rsi - current string pointer
  xor r14, r14
  mov r15, rsp

  ; add cards from [0, 211)
  ; r13 - index
  xor r13, r13
  init_cards:
    dec rsp
	mov [rsp], r13b
	inc r13
	cmp r13, 211
	jne init_cards

  handle_cards:
    ; r13 - current card
	movzx r13, byte [rsp]
	inc rsp
	inc r14
	; r12 - start of winning numbers
	; 118 - card line length including card id and \n
	mov rax, 117
	mul r13
	add rax, _binary_input_txt_start
    add rax, 10 ; skip "Card ###: " in the string
	mov r12, rax

    ; r13 - next card id to earn
    ; r11 - start of selected numbers
    ; r10 - winning number index in r12
    ; r9 - selected number index in r11
	inc r13
    lea r11, [r12 + 32]
    xor r10, r10
  
    search_winning_numbers:
      ; r8w - winning number as characters
      mov r8w, [r12 + r10]
      xor r9, r9
  
      search_selected_numbers:
  	  cmp r8w, [r9 + r11]
  	  je got_winning_number
  	  add r9, 3
  	  cmp r9, 75
  	  je next_winning_number
  	  jmp search_selected_numbers
  
  	got_winning_number:
	  ; add the new card
	  dec rsp
	  mov [rsp], r13b
	  inc r13
  
      next_winning_number:
      add r10, 3
  	cmp r10, 30
  	jne search_winning_numbers

	cmp rsp, r15
	jne handle_cards

  nop
  mov rsi, r14
  call print_number

  ; exit(0)
  mov rax, 60
  mov rdi, 0
  syscall
